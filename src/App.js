import React, { Component, Fragment } from 'react';
import {Switch, Route,} from "react-router-dom";
import SubmitNewQuote from "./containers/SubmitNewQuote";
import QuotesList from "./containers/QuotesList";
import EditQuote from "./containers/EditQuotes";
import {Container} from "reactstrap"
import NavMenu from "./components/NavMenu";
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="app-kek upc-wrapper">
          <NavMenu/>
          <Container>
              <Switch>
                  <Route path='/' exact component={QuotesList}/>
                  <Route path='/add-quote' exact component={SubmitNewQuote}/>
                  <Route path='/quotes/:id/edit' component={EditQuote}/>
                  <Route path='/quotes/:categoryId' component={QuotesList}/>
              </Switch>
          </Container>
      </div>
    );
  }
}

export default App;
