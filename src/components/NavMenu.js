import React from 'react';
import {Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

const NavMenu = () => {
    return (
        <Navbar expand="md">
            <NavbarBrand className="navbar-title" >Экстренная попячка нужжающися</NavbarBrand>
            <NavbarToggler/>
            <Collapse isOpen navbar>
                <Nav className="ml-auto" navbar>
                    <NavItem>
                        <NavLink className="upc-nav-link" tag={RouterNavLink} to='/' exact>Почитать чо рассказывают</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink className="upc-nav-link" tag={RouterNavLink} to='/add-quote'>Жахнуть по УГ</NavLink>
                    </NavItem>
                </Nav>
            </Collapse>
        </Navbar>
    );
};

export default NavMenu;