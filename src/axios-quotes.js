import axios from 'axios';

const instance = axios.create({
    baseURL: "https://exam8-lol-kek-cheburek.firebaseio.com/"
});
export default instance